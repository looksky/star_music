package com.zxhhyj.music.ui.theme

import androidx.compose.foundation.shape.RoundedCornerShape

val roundShape = RoundedCornerShape(round)