package com.zxhhyj.music.ui.screen.setting

import android.content.pm.PackageManager
import android.os.Build
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ColorLens
import androidx.compose.material.icons.rounded.Info
import androidx.compose.material.icons.rounded.Source
import androidx.compose.material.icons.rounded.TextFormat
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.zxhhyj.music.R
import com.zxhhyj.music.logic.config.application
import com.zxhhyj.music.ui.common.TopAppBar
import com.zxhhyj.music.ui.common.bindTopAppBarState
import com.zxhhyj.music.ui.common.rememberTopAppBarState
import com.zxhhyj.music.ui.screen.ScreenDestination
import com.zxhhyj.music.ui.screen.setting.item.SettingItem
import dev.olshevski.navigation.reimagined.NavController
import dev.olshevski.navigation.reimagined.navigate

@Composable
fun SettingScreen(
    mainNavController: NavController<ScreenDestination>,
    padding: PaddingValues,
) {
    val versionName = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        application.packageManager.getPackageInfo(
            application.packageName,
            PackageManager.PackageInfoFlags.of(0)
        )
    } else {
        application.packageManager.getPackageInfo(application.packageName, 0)
    }.versionName
    val topAppBarState = rememberTopAppBarState()
    LazyColumn(
        modifier = Modifier
            .bindTopAppBarState(topAppBarState)
            .fillMaxSize()
            .padding(padding)
    ) {
        item {
            SettingItem(
                imageVector = Icons.Rounded.ColorLens,
                title = stringResource(id = R.string.theme),
                subTitle = stringResource(id = R.string.theme_info)
            ) {
                mainNavController.navigate(ScreenDestination.Theme)
            }
        }
        item {
            SettingItem(
                imageVector = Icons.Rounded.Source,
                title = stringResource(id = R.string.media_lib),
                subTitle = stringResource(id = R.string.scan_music)
            ) {
                mainNavController.navigate(ScreenDestination.MediaSource)
            }
        }
        item {
            SettingItem(
                imageVector = Icons.Rounded.TextFormat,
                title = stringResource(id = R.string.lyric),
                subTitle = stringResource(id = R.string.lyric_info)
            ) {

            }
        }
        item {
            SettingItem(
                imageVector = Icons.Rounded.Info,
                title = stringResource(id = R.string.about),
                subTitle = stringResource(
                    id = R.string.about_info,
                    formatArgs = arrayOf(versionName)
                )
            ) {
                mainNavController.navigate(ScreenDestination.About)
            }
        }
    }
    TopAppBar(
        state = topAppBarState,
        modifier = Modifier.fillMaxWidth(),
        title = stringResource(id = R.string.setting)
    )
}