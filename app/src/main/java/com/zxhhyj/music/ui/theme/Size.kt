package com.zxhhyj.music.ui.theme

import androidx.compose.ui.unit.dp

val vertical = 10.dp

val horizontal = 16.dp

val round = 8.dp

