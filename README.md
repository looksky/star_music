<p align="center"><img src="https://gitee.com/ZXHHYJ/mandysa_music/raw/master/app/src/main/res/mipmap-xxxhdpi/ic_launcher_round.png"/></p>

<p align="center">
    <strong>Hello MandySa Music!</strong>
    <br>
    <br>
    <a href="https://gitee.com/ZXHHYJ/mandysa_music/wikis">Wiki</a>
</p>

<p align="center">
<img src="https://img.shields.io/badge/language-kotlin-orange.svg"/>
<img src="https://img.shields.io/badge/license-Apache-blue"/>
<a href="https://gitee.com/ZXHHYJ/mandysa_music/releases"><img src="https://img.shields.io/badge/updates-%E6%9B%B4%E6%96%B0%E6%97%A5%E5%BF%97-brightgreen"/></a>
</p>

<br>
<p align="center"><strong>基于compose实现的音乐播放器</strong></p>
<br>

<br>
<p align="center"><strong>欢迎贡献代码/问题</strong></p>
<br>

## 手机界面截图

|![](https://gitee.com/ZXHHYJ/mandysa_music/raw/master/screen/手机-主页.png)|![](https://gitee.com/ZXHHYJ/mandysa_music/raw/master/screen/手机-播放页.png)|
|--|--|
|![](https://gitee.com/ZXHHYJ/mandysa_music/raw/master/screen/手机-歌词.png)

## 平板界面截图

![](https://gitee.com/ZXHHYJ/mandysa_music/raw/master/screen/平板-主页.png)![](https://gitee.com/ZXHHYJ/mandysa_music/raw/master/screen/平板-播放页和歌词.png)

## License

```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
